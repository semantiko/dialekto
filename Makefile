all:
	#dune build main.exe
	dune build
	#cp _build/default/main.exe ./DiaLeKTo

test:
	dune runtest

doc:
	dune build @doc

clean:
	dune clean
